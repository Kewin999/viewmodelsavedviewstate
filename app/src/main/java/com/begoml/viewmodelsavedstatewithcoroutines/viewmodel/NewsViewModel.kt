package com.begoml.viewmodelsavedstatewithcoroutines.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.begoml.viewmodelsavedstatewithcoroutines.core.NewsRepository
import com.begoml.viewmodelsavedstatewithcoroutines.core.model.NewsModel
import com.begoml.viewmodelsavedstatewithcoroutines.tools.handleResults
import kotlinx.coroutines.launch

class NewsViewModel constructor(
    private val savedStateHandle: SavedStateHandle,
    private val newsRepository: NewsRepository
) : ViewStateViewModel<ViewState>(ViewState()) {

    init {
        savedStateHandle.get<List<NewsModel>>(KEY_NEWS)?.let { value ->
            onNextViewState(
                currentStateValue.copy(
                    news = value
                )
            )
        }
    }

    override val viewState: LiveData<ViewState> by lazy {
        updateNewsIfNeed()
        currentState
    }

    private fun updateNewsIfNeed() {
        if (currentStateValue.news.isEmpty()) {

            onNextViewState(currentStateValue.copy(isLoading = true))

            viewModelScope.launch {
                newsRepository.getNews().handleResults(
                    {
                        onNextViewState(
                            currentStateValue.copy(
                                isLoading = false
                            )
                        )
                    }, {
                        savedStateHandle.set(KEY_NEWS, it)
                        onNextViewState(
                            currentStateValue.copy(
                                news = it,
                                isLoading = false
                            )
                        )
                    }
                )
            }
        }
    }

    companion object {
        private const val KEY_NEWS = "key_news"
    }
}

data class ViewState(
    val isLoading: Boolean = false,
    val news: List<NewsModel> = emptyList()
)