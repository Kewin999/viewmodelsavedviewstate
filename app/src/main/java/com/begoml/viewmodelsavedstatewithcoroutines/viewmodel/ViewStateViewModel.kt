package com.begoml.viewmodelsavedstatewithcoroutines.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class ViewStateViewModel<ViewState : Any>(state: ViewState) : ViewModel() {

    /**
     * has state for View
     */
    private val state: MutableLiveData<ViewState> by lazy {
        MutableLiveData<ViewState>().apply {
            value = state
        }
    }

    open val viewState: LiveData<ViewState> by lazy {
        currentState
    }

    protected fun onNextViewState(viewState: ViewState) {
        state.value = viewState
    }

    protected val currentState
        get() = state as LiveData<ViewState>

    protected val currentStateValue
        get() = state.value!!
}