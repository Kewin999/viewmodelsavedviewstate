package com.begoml.viewmodelsavedstatewithcoroutines

import android.app.Application
import com.begoml.viewmodelsavedstatewithcoroutines.core.NewsRepository
import com.begoml.viewmodelsavedstatewithcoroutines.di.main.AppComponent
import com.begoml.viewmodelsavedstatewithcoroutines.di.main.AppProvider
import javax.inject.Inject

class NewsApplication : Application(), BaseApp {

    @Inject
    lateinit var repo: NewsRepository

    override fun onCreate() {
        super.onCreate()

        AppComponent.init(applicationContext)
        AppComponent.get().inject(this)
    }

    override fun getAppProvider(): AppProvider {
        return AppComponent.get()
    }
}

interface BaseApp {

    fun getAppProvider(): AppProvider
}
