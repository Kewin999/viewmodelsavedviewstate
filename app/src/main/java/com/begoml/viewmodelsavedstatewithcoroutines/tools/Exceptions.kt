package com.begoml.viewmodelsavedstatewithcoroutines.tools

class InitComponentException: RuntimeException("You must call 'init' method")