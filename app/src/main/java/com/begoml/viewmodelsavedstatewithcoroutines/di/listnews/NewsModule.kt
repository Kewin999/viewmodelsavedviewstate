package com.begoml.viewmodelsavedstatewithcoroutines.di.listnews

import androidx.lifecycle.ViewModelProvider
import com.begoml.viewmodelsavedstatewithcoroutines.viewmodel.NewsViewModelFactory
import dagger.Binds
import dagger.Module

@Module
interface NewsModule {

    @Binds
    fun bindsViewModelFactory(factory: NewsViewModelFactory): ViewModelProvider.Factory
}