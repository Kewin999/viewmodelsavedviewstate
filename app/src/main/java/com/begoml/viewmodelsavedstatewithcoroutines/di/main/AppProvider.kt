package com.begoml.viewmodelsavedstatewithcoroutines.di.main

import android.content.Context
import com.begoml.viewmodelsavedstatewithcoroutines.core.NewsRepository

interface AppProvider {

    fun provideContext(): Context

    fun provideNewsRepository(): NewsRepository
}