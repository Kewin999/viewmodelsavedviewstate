package com.begoml.viewmodelsavedstatewithcoroutines.di.main

import com.begoml.viewmodelsavedstatewithcoroutines.core.NewsRepository
import com.begoml.viewmodelsavedstatewithcoroutines.core.NewsRepositoryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface RepoModule {

    @Binds
    @Singleton
    fun provides(impl: NewsRepositoryImpl): NewsRepository

}