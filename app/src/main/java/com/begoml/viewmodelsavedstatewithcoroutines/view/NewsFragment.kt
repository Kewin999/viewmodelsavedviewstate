package com.begoml.viewmodelsavedstatewithcoroutines.view

import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.begoml.viewmodelsavedstatewithcoroutines.BaseApp
import com.begoml.viewmodelsavedstatewithcoroutines.R
import com.begoml.viewmodelsavedstatewithcoroutines.di.listnews.DaggerNewsComponent
import com.begoml.viewmodelsavedstatewithcoroutines.di.listnews.NewsComponent
import com.begoml.viewmodelsavedstatewithcoroutines.tools.initializeViewStateWatcher
import com.begoml.viewmodelsavedstatewithcoroutines.viewmodel.NewsViewModel
import com.begoml.viewmodelsavedstatewithcoroutines.viewmodel.ViewState
import kotlinx.android.synthetic.main.news_fragment.*

class NewsFragment : ViewStateFragment<NewsViewModel>(
    R.layout.news_fragment,
    NewsViewModel::class.java
) {

    private lateinit var newsComponent: NewsComponent

    private val newsAdapter by lazy { NewsAdapter() }

    override val watcher = initializeViewStateWatcher<ViewState> {

        ViewState::isLoading { isListLoading ->
            progress.visibility(isListLoading)
        }

        ViewState::news{
            newsAdapter.submitList(it)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        list.apply {
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(ItemDecoration())
            adapter = newsAdapter
        }
    }

    override fun injectDependencies() {
        if (!::newsComponent.isInitialized) {
            DaggerNewsComponent.builder()
                .screen(this@NewsFragment)
                .appComponent((context?.applicationContext as BaseApp).getAppProvider())
                .build().apply {
                    newsComponent = this
                }
        }
        newsComponent.inject(this)
    }
}

fun View.visibility(visible: Boolean) {
    visibility = if (visible) View.VISIBLE else View.GONE
}

class ItemDecoration: RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)

        outRect.top = parent.context.resources.getDimensionPixelSize(R.dimen.margin_20)
        outRect.right = parent.context.resources.getDimensionPixelSize(R.dimen.margin_8)
        outRect.left = parent.context.resources.getDimensionPixelSize(R.dimen.margin_8)
    }
}