package com.begoml.viewmodelsavedstatewithcoroutines.view

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.begoml.viewmodelsavedstatewithcoroutines.R
import com.begoml.viewmodelsavedstatewithcoroutines.core.model.NewsModel
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_news.*

class NewsAdapter : BaseRecyclerAdapter<NewsModel, NewsAdapter.NewsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        return NewsViewHolder(inflate(R.layout.item_news, parent))
    }

    override fun submitList(newList: List<NewsModel>) {
        val oldList = items
        notify(oldList, newList) { o, n -> o.id == n.id }
        items = newList
    }

    inner class NewsViewHolder(override val containerView: View):
        BaseViewHolder<NewsModel>(containerView), LayoutContainer {

        override fun bind(data: NewsModel) {
            newsTitle.text = data.title
            newsDesc.text = data.description
        }
    }
}

fun <T, VH> RecyclerView.Adapter<VH>.notify(
    old: List<T>,
    new: List<T>,
    compare: (T, T) -> Boolean
) where VH : RecyclerView.ViewHolder {
    val diff = DiffUtil.calculateDiff(object : DiffUtil.Callback() {

        /**
         * By default you should compare item's id to be sure that items are identical
         */
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return compare(old[oldItemPosition], new[newItemPosition])
        }

        /**
         * To use this comparison properly override [equals] method or use data class
         */
        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return old[oldItemPosition] == new[newItemPosition]
        }

        override fun getOldListSize() = old.size

        override fun getNewListSize() = new.size
    })

    diff.dispatchUpdatesTo(this)
}