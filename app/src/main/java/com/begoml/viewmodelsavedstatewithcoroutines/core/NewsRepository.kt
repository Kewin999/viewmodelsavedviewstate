package com.begoml.viewmodelsavedstatewithcoroutines.core

import com.begoml.viewmodelsavedstatewithcoroutines.core.model.NewsModel
import com.begoml.viewmodelsavedstatewithcoroutines.tools.ResultWrapper

interface NewsRepository {

    suspend fun getNews(): ResultWrapper<Throwable, List<NewsModel>>
}